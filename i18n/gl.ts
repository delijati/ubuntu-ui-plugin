<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>AccountSettingsList</name>
    <message>
        <location filename="../plugins/core/mail/settings/AccountSettingsList.qml" line="26"/>
        <source>Account Settings</source>
        <translation type="unfinished">Axustes da conta</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/AccountSettingsList.qml" line="56"/>
        <source>Details</source>
        <translation type="unfinished">Detalles</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/AccountSettingsList.qml" line="60"/>
        <source>Incoming Server</source>
        <translation type="unfinished">Servidor entrante</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/AccountSettingsList.qml" line="64"/>
        <source>Outgoing Server</source>
        <translation type="unfinished">Servidor saínte</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/AccountSettingsList.qml" line="68"/>
        <source>Copies and Folders</source>
        <translation type="unfinished">Copias e cartafoles</translation>
    </message>
</context>
<context>
    <name>AddAnotherUI</name>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/AddAnotherUI.qml" line="30"/>
        <source>Success</source>
        <translation type="unfinished">Rematado correctamente</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/AddAnotherUI.qml" line="89"/>
        <source>New account created.</source>
        <translation type="unfinished">Creouse a conta nova.</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/AddAnotherUI.qml" line="98"/>
        <source>Continue</source>
        <translation type="unfinished">Continuar</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/AddAnotherUI.qml" line="113"/>
        <source>Add another</source>
        <translation type="unfinished">Engadir outra</translation>
    </message>
</context>
<context>
    <name>AddressBookList</name>
    <message>
        <location filename="../plugins/core/mail/contacts/AddressBookList.qml" line="12"/>
        <source>Addressbooks</source>
        <translation type="unfinished">Axendas</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/contacts/AddressBookList.qml" line="65"/>
        <source>Add Collection</source>
        <translation type="unfinished">Engadir colección</translation>
    </message>
</context>
<context>
    <name>AddressBookStage</name>
    <message>
        <location filename="../plugins/core/contacts/AddressBookStage.qml" line="34"/>
        <source>Coming soon</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AttachmentPanel</name>
    <message>
        <location filename="../plugins/core/mail/components/AttachmentPanel.qml" line="81"/>
        <source>Attachments</source>
        <translation type="unfinished">Anexos</translation>
    </message>
</context>
<context>
    <name>AttachmentPopover</name>
    <message>
        <location filename="../plugins/core/mail/popovers/AttachmentPopover.qml" line="46"/>
        <source>Delete</source>
        <translation type="unfinished">Eliminar</translation>
    </message>
</context>
<context>
    <name>AttachmentsStage</name>
    <message>
        <location filename="../plugins/extensions/attachments/AttachmentsStage.qml" line="34"/>
        <source>Coming soon</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AuthenticationSelector</name>
    <message>
        <location filename="../plugins/core/mail/components/AuthenticationSelector.qml" line="45"/>
        <source>Authentication</source>
        <translation type="unfinished">Autenticación</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/components/AuthenticationSelector.qml" line="60"/>
        <source>PLAIN</source>
        <translation type="unfinished">PLANO</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/components/AuthenticationSelector.qml" line="61"/>
        <source>LOGIN</source>
        <translation type="unfinished">INICIAR SESIÓN</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/components/AuthenticationSelector.qml" line="62"/>
        <source>CRAM-MD5</source>
        <translation type="unfinished">CRAM-MD5</translation>
    </message>
</context>
<context>
    <name>AutoConfigState</name>
    <message>
        <location filename="../plugins/core/mail/setupwizard/states/AutoConfigState.qml" line="35"/>
        <source>Searching for configuration.</source>
        <translation type="unfinished">Buscando a configuración</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/states/AutoConfigState.qml" line="77"/>
        <source>IMAP server found</source>
        <translation type="unfinished">Atopouse o servidor IMAP</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/states/AutoConfigState.qml" line="78"/>
        <source>A IMAP server configuration was found for your domain.

Would you like to use this instead?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BottomEdgeComposer</name>
    <message>
        <location filename="../plugins/core/mail/composer/BottomEdgeComposer.qml" line="65"/>
        <source>Attachments</source>
        <translation type="unfinished">Anexos</translation>
    </message>
</context>
<context>
    <name>CalendarStage</name>
    <message>
        <location filename="../plugins/core/calendar/CalendarStage.qml" line="34"/>
        <source>Coming soon</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ComposeWindow</name>
    <message>
        <location filename="../plugins/core/mail/composer/ComposeWindow.qml" line="24"/>
        <source>Dekko Composer</source>
        <translation type="unfinished">Editor de Dekko</translation>
    </message>
</context>
<context>
    <name>Composer</name>
    <message>
        <location filename="../plugins/core/mail/composer/Composer.qml" line="53"/>
        <source>Attach</source>
        <translation type="unfinished">Anexar</translation>
    </message>
</context>
<context>
    <name>ConfirmationDialog</name>
    <message>
        <location filename="../imports/dialogs/ConfirmationDialog.qml" line="50"/>
        <source>Cancel</source>
        <translation type="unfinished">Cancelar</translation>
    </message>
    <message>
        <location filename="../imports/dialogs/ConfirmationDialog.qml" line="63"/>
        <source>Confirm</source>
        <translation type="unfinished">Confirmar</translation>
    </message>
</context>
<context>
    <name>ContactFilterView</name>
    <message>
        <location filename="../plugins/core/mail/views/ContactFilterView.qml" line="112"/>
        <source>Add contact</source>
        <translation type="unfinished">Engadir contacto</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/views/ContactFilterView.qml" line="123"/>
        <source>Send message</source>
        <translation type="unfinished">Enviar mensaxe</translation>
    </message>
</context>
<context>
    <name>ContactListPage</name>
    <message>
        <location filename="../plugins/core/mail/contacts/ContactListPage.qml" line="11"/>
        <source>Address book</source>
        <translation type="unfinished">Axenda</translation>
    </message>
</context>
<context>
    <name>ContactView</name>
    <message>
        <location filename="../plugins/core/mail/contacts/ContactView.qml" line="11"/>
        <source>Contact</source>
        <translation type="unfinished">Contacto</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/contacts/ContactView.qml" line="61"/>
        <source>Email</source>
        <translation type="unfinished">Correo electrónico</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/contacts/ContactView.qml" line="78"/>
        <source>Phone</source>
        <translation type="unfinished">Teléfono</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/contacts/ContactView.qml" line="92"/>
        <source>Address</source>
        <translation type="unfinished">Enderezo</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/contacts/ContactView.qml" line="96"/>
        <source>Street</source>
        <translation type="unfinished">Rúa</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/contacts/ContactView.qml" line="102"/>
        <source>City</source>
        <translation type="unfinished">Cidade</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/contacts/ContactView.qml" line="108"/>
        <source>Zip</source>
        <translation type="unfinished">CP</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/contacts/ContactView.qml" line="114"/>
        <source>Country</source>
        <translation type="unfinished">País</translation>
    </message>
</context>
<context>
    <name>ContactsListView</name>
    <message>
        <location filename="../plugins/core/mail/contacts/ContactsListView.qml" line="36"/>
        <source>Search</source>
        <translation type="unfinished">Buscar</translation>
    </message>
</context>
<context>
    <name>ContentBlockedNotice</name>
    <message>
        <location filename="../plugins/core/mail/webview/ContentBlockedNotice.qml" line="41"/>
        <source>Remote content blocked</source>
        <translation type="unfinished">Contido remoto bloqueado</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/webview/ContentBlockedNotice.qml" line="54"/>
        <source>Allow</source>
        <translation type="unfinished">Permitir</translation>
    </message>
</context>
<context>
    <name>ContributorsPage</name>
    <message>
        <location filename="../plugins/core/mail/views/ContributorsPage.qml" line="25"/>
        <source>Contributors</source>
        <translation type="unfinished">Contribuidores</translation>
    </message>
</context>
<context>
    <name>CopyFoldersGroup</name>
    <message>
        <location filename="../plugins/core/mail/settings/CopyFoldersGroup.qml" line="30"/>
        <source>Copies and Folders</source>
        <translation type="unfinished">Copias e cartafoles</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/CopyFoldersGroup.qml" line="124"/>
        <source>Standard folders</source>
        <translation type="unfinished">Cartafoles estándar</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/CopyFoldersGroup.qml" line="136"/>
        <source>Detect standard folders</source>
        <translation type="unfinished">Detectar cartafoles estándar</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/CopyFoldersGroup.qml" line="152"/>
        <source>Detect</source>
        <translation type="unfinished">Detectar</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/CopyFoldersGroup.qml" line="173"/>
        <source>Base folder</source>
        <translation type="unfinished">Cartafol base</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/CopyFoldersGroup.qml" line="175"/>
        <source>Leave empty if you are unsure</source>
        <translation type="unfinished">Déixeo baleiro se non esta seguro</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/CopyFoldersGroup.qml" line="190"/>
        <source>Inbox folder</source>
        <translation type="unfinished">Cartafol da caixa de entrada</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/CopyFoldersGroup.qml" line="205"/>
        <source>Drafts folder</source>
        <translation type="unfinished">Cartafol dos borradores</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/CopyFoldersGroup.qml" line="220"/>
        <source>Spam folder</source>
        <translation type="unfinished">Cartafol do correo lixo</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/CopyFoldersGroup.qml" line="235"/>
        <source>Sent folder</source>
        <translation type="unfinished">Cartafol de enviados</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/CopyFoldersGroup.qml" line="250"/>
        <source>Outbox folder</source>
        <translation type="unfinished">Cartafol da caixa de saída</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/CopyFoldersGroup.qml" line="265"/>
        <source>Trash folder</source>
        <translation type="unfinished">Cartafol do lixo</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/CopyFoldersGroup.qml" line="274"/>
        <source>Sending messages</source>
        <translation type="unfinished">Enviando mensaxes</translation>
    </message>
</context>
<context>
    <name>DefaultMessagePage</name>
    <message>
        <location filename="../plugins/core/mail/messageview/DefaultMessagePage.qml" line="182"/>
        <source>From:</source>
        <translation type="unfinished">De:</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/messageview/DefaultMessagePage.qml" line="235"/>
        <source>To:</source>
        <translation type="unfinished">A:</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/messageview/DefaultMessagePage.qml" line="240"/>
        <source>Cc:</source>
        <translation type="unfinished">Cc:</translation>
    </message>
</context>
<context>
    <name>DefaultPlugin</name>
    <message>
        <location filename="../plugins/extensions/addressbook/DefaultPlugin.qml" line="11"/>
        <source>Internal</source>
        <translation type="unfinished">Interno</translation>
    </message>
    <message>
        <location filename="../plugins/extensions/addressbook/DefaultPlugin.qml" line="106"/>
        <source>Default</source>
        <translation type="unfinished">Predeterminado</translation>
    </message>
</context>
<context>
    <name>DekkoHeader</name>
    <message>
        <location filename="../imports/components/DekkoHeader.qml" line="204"/>
        <source>Enter search...</source>
        <translation type="unfinished">Escribir a busca...</translation>
    </message>
</context>
<context>
    <name>DekkoWebView</name>
    <message>
        <location filename="../plugins/core/mail/webview/DekkoWebView.qml" line="142"/>
        <source>Open in browser?</source>
        <translation type="unfinished">Abrir no navegador?</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/webview/DekkoWebView.qml" line="143"/>
        <source>Confirm to open %1 in web browser</source>
        <translation type="unfinished">Confirmar para abrir %1 no navegador web</translation>
    </message>
</context>
<context>
    <name>DetailList</name>
    <message>
        <location filename="../plugins/core/mail/messageview/DetailList.qml" line="47"/>
        <source>Details</source>
        <translation type="unfinished">Detalles</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/messageview/DetailList.qml" line="52"/>
        <source>To:</source>
        <translation type="unfinished">A:</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/messageview/DetailList.qml" line="58"/>
        <source>Cc:</source>
        <translation type="unfinished">Cc:</translation>
    </message>
</context>
<context>
    <name>DetailsGroup</name>
    <message>
        <location filename="../plugins/core/mail/settings/DetailsGroup.qml" line="27"/>
        <source>Details</source>
        <translation type="unfinished">Detalles</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/DetailsGroup.qml" line="62"/>
        <source>Account name</source>
        <translation type="unfinished">Nome da conta</translation>
    </message>
</context>
<context>
    <name>DisplaySettings</name>
    <message>
        <location filename="../plugins/core/mail/settings/DisplaySettings.qml" line="28"/>
        <source>Navigation menu</source>
        <translation type="unfinished">Menú de navegación</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/DisplaySettings.qml" line="32"/>
        <source>Show smart folders</source>
        <translation type="unfinished">Mostrar cartafoles intelixentes</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/DisplaySettings.qml" line="42"/>
        <source>Show favourite folders</source>
        <translation type="unfinished">Mostrar cartafoles favoritos</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/DisplaySettings.qml" line="53"/>
        <source>Messages</source>
        <translation type="unfinished">Mensaxes</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/DisplaySettings.qml" line="57"/>
        <source>Show avatars</source>
        <translation type="unfinished">Mostrar avatares</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/DisplaySettings.qml" line="67"/>
        <source>Prefer plain text</source>
        <translation type="unfinished">Preferir texto plano</translation>
    </message>
</context>
<context>
    <name>DisplaySettingsPage</name>
    <message>
        <location filename="../plugins/core/mail/settings/DisplaySettingsPage.qml" line="21"/>
        <source>Display Settings</source>
        <translation type="unfinished">Mostrar axustes</translation>
    </message>
</context>
<context>
    <name>DisplaySettingsPopup</name>
    <message>
        <location filename="../plugins/core/mail/settings/DisplaySettingsPopup.qml" line="21"/>
        <source>Display Settings</source>
        <translation type="unfinished">Mostrar axustes</translation>
    </message>
</context>
<context>
    <name>EncryptionSelector</name>
    <message>
        <location filename="../plugins/core/mail/components/EncryptionSelector.qml" line="46"/>
        <source>Encryption</source>
        <translation type="unfinished">Cifrado</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/components/EncryptionSelector.qml" line="61"/>
        <source>No encryption</source>
        <translation type="unfinished">Sen cifrado</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/components/EncryptionSelector.qml" line="62"/>
        <source>Use encryption (STARTTLS)</source>
        <translation type="unfinished">Usar cifrado (STARTTLS)</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/components/EncryptionSelector.qml" line="63"/>
        <source>Force encryption (SSL/TLS)</source>
        <translation type="unfinished">Forzar cifrado (SSL/TLS)</translation>
    </message>
</context>
<context>
    <name>ExpandablePanel</name>
    <message>
        <location filename="../imports/components/ExpandablePanel.qml" line="62"/>
        <source>Attachments</source>
        <translation type="unfinished">Anexos</translation>
    </message>
</context>
<context>
    <name>FilePickerDialog</name>
    <message>
        <location filename="../imports/dialogs/FilePickerDialog.qml" line="23"/>
        <source>Add Attachment</source>
        <translation type="unfinished">Engadir anexo</translation>
    </message>
</context>
<context>
    <name>FolderListDelegate</name>
    <message>
        <location filename="../plugins/core/mail/delegates/FolderListDelegate.qml" line="52"/>
        <source>Un-favourite</source>
        <translation type="unfinished">Non favorito</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/delegates/FolderListDelegate.qml" line="52"/>
        <source>Favourite</source>
        <translation type="unfinished">Favorito</translation>
    </message>
</context>
<context>
    <name>HtmlViewer</name>
    <message>
        <location filename="../plugins/extensions/html-viewer/HtmlViewer.qml" line="11"/>
        <source>HTML Viewer</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>IdentitiesListPage</name>
    <message>
        <location filename="../plugins/core/mail/settings/IdentitiesListPage.qml" line="14"/>
        <source>Identities</source>
        <translation type="unfinished">Identidades</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IdentitiesListPage.qml" line="106"/>
        <source> (Default)</source>
        <translation type="unfinished"> (Predeterminado)</translation>
    </message>
</context>
<context>
    <name>IdentityInput</name>
    <message>
        <location filename="../plugins/core/mail/settings/IdentityInput.qml" line="54"/>
        <source>Default identity</source>
        <translation type="unfinished">Identidade predeterminada</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IdentityInput.qml" line="67"/>
        <source>Account</source>
        <translation type="unfinished">Conta</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IdentityInput.qml" line="105"/>
        <source>Name</source>
        <translation type="unfinished">Nome</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IdentityInput.qml" line="110"/>
        <source>Email Address</source>
        <translation type="unfinished">Enderezo de correo-e</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IdentityInput.qml" line="115"/>
        <source>Reply-To</source>
        <translation type="unfinished">Responder a</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IdentityInput.qml" line="119"/>
        <source>Signature</source>
        <translation type="unfinished">Sinatura</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IdentityInput.qml" line="137"/>
        <location filename="../plugins/core/mail/settings/IdentityInput.qml" line="145"/>
        <source>New identity</source>
        <translation type="unfinished">Nova identidade</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IdentityInput.qml" line="169"/>
        <source>Edit identity</source>
        <translation type="unfinished">Editar identidade</translation>
    </message>
</context>
<context>
    <name>IncomingServerGroup</name>
    <message>
        <location filename="../plugins/core/mail/settings/IncomingServerGroup.qml" line="28"/>
        <source>Incoming Server</source>
        <translation type="unfinished">Servidor entrante</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IncomingServerGroup.qml" line="105"/>
        <source>Hostname</source>
        <translation type="unfinished">Nome do equipo</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IncomingServerGroup.qml" line="114"/>
        <source>Port</source>
        <translation type="unfinished">Porto</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IncomingServerGroup.qml" line="123"/>
        <source>Username</source>
        <translation type="unfinished">Nome do usuario</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IncomingServerGroup.qml" line="132"/>
        <location filename="../plugins/core/mail/settings/IncomingServerGroup.qml" line="135"/>
        <source>Password</source>
        <translation type="unfinished">Contrasinal</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IncomingServerGroup.qml" line="142"/>
        <source>Show password</source>
        <translation type="unfinished">Mostrar o contrasinal</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IncomingServerGroup.qml" line="148"/>
        <source>Security settings</source>
        <translation type="unfinished">Axustes de seguranza</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IncomingServerGroup.qml" line="183"/>
        <source>Allow untrusted certificates</source>
        <translation type="unfinished">Permitir certificados sen confianza</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IncomingServerGroup.qml" line="188"/>
        <source>Server settings</source>
        <translation type="unfinished">Axustes do servidor</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IncomingServerGroup.qml" line="193"/>
        <source>Check for new mail on start</source>
        <translation type="unfinished">Buscar correo novo ao iniciar</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IncomingServerGroup.qml" line="199"/>
        <source>Enable IMAP IDLE</source>
        <translation type="unfinished">Activar IMAP IDLE</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IncomingServerGroup.qml" line="206"/>
        <source>Check interval (minutes)</source>
        <translation type="unfinished">Intervalo de comprobación (minutos)</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IncomingServerGroup.qml" line="213"/>
        <source>Check when roaming</source>
        <translation type="unfinished">Comprobar en itinerancia</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IncomingServerGroup.qml" line="219"/>
        <source>Maximum mail size (MB)</source>
        <translation type="unfinished">Tamaño máximo do correo (MB)</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IncomingServerGroup.qml" line="225"/>
        <source>No maximum mail size</source>
        <translation type="unfinished">Sen tamaño máximo de correo</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IncomingServerGroup.qml" line="233"/>
        <source>Automatically download attachments</source>
        <translation type="unfinished">Descargar anexos automaticamente</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/IncomingServerGroup.qml" line="239"/>
        <source>Allowed to delete mail</source>
        <translation type="unfinished">Permitir eliminar correos</translation>
    </message>
</context>
<context>
    <name>LicensesPage</name>
    <message>
        <location filename="../plugins/core/mail/views/LicensesPage.qml" line="25"/>
        <source>Licenses</source>
        <translation type="unfinished">Licenzas</translation>
    </message>
</context>
<context>
    <name>MailSettings</name>
    <message>
        <location filename="../plugins/core/mail/settings/MailSettings.qml" line="11"/>
        <source>Mail Settings</source>
        <translation type="unfinished">Axustes do correo</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/MailSettings.qml" line="24"/>
        <source>Accounts</source>
        <translation type="unfinished">Contas</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/MailSettings.qml" line="34"/>
        <source>Identities</source>
        <translation type="unfinished">Identidades</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/MailSettings.qml" line="44"/>
        <source>Display</source>
        <translation type="unfinished">Visualización</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/MailSettings.qml" line="51"/>
        <source>Privacy</source>
        <translation type="unfinished">Confidencialidade</translation>
    </message>
</context>
<context>
    <name>MailSettingsAction</name>
    <message>
        <location filename="../plugins/core/mail/settings/MailSettingsAction.qml" line="7"/>
        <source>Mail</source>
        <translation type="unfinished">Correo</translation>
    </message>
</context>
<context>
    <name>MailUtils</name>
    <message>
        <location filename="../imports/constants/MailUtils.qml" line="27"/>
        <source>To</source>
        <translation type="unfinished">Para</translation>
    </message>
    <message>
        <location filename="../imports/constants/MailUtils.qml" line="29"/>
        <source>Cc</source>
        <translation type="unfinished">Cc</translation>
    </message>
    <message>
        <location filename="../imports/constants/MailUtils.qml" line="31"/>
        <source>Bcc</source>
        <translation type="unfinished">Cco</translation>
    </message>
</context>
<context>
    <name>MailboxPickerPage</name>
    <message>
        <location filename="../plugins/core/mail/views/MailboxPickerPage.qml" line="34"/>
        <source>Select folder</source>
        <translation type="unfinished">Seleccionar cartafol</translation>
    </message>
</context>
<context>
    <name>MainUI</name>
    <message>
        <location filename="../qml/MainUI.qml" line="17"/>
        <source>Dekko Mail</source>
        <translation type="unfinished">Correo Dekko</translation>
    </message>
</context>
<context>
    <name>ManageAccountsPage</name>
    <message>
        <location filename="../plugins/core/mail/settings/ManageAccountsPage.qml" line="26"/>
        <source>Manage accounts</source>
        <translation type="unfinished">Xestionar contas</translation>
    </message>
</context>
<context>
    <name>ManualInputUI</name>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/ManualInputUI.qml" line="28"/>
        <source>Server configuration</source>
        <translation type="unfinished">Configuración do servidor</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/ManualInputUI.qml" line="33"/>
        <source>IMAP Server:</source>
        <translation type="unfinished">Servidor IMAP:</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/ManualInputUI.qml" line="46"/>
        <source>POP3 Server:</source>
        <translation type="unfinished">Servidor POP3:</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/ManualInputUI.qml" line="59"/>
        <source>SMTP Server:</source>
        <translation type="unfinished">Servidor SMTP:</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/ManualInputUI.qml" line="71"/>
        <source>Back</source>
        <translation type="unfinished">Volver</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/ManualInputUI.qml" line="75"/>
        <source>Next</source>
        <translation type="unfinished">Seguinte</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/ManualInputUI.qml" line="135"/>
        <source>Password empty</source>
        <translation type="unfinished">Contrasinal en branco</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/ManualInputUI.qml" line="135"/>
        <source>Would you like to continue?</source>
        <translation type="unfinished">Desexa continuar?</translation>
    </message>
</context>
<context>
    <name>MarkdownEditor</name>
    <message>
        <location filename="../plugins/extensions/Markdown/MarkdownEditor.qml" line="66"/>
        <source>Preview</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MessageActionPopover</name>
    <message>
        <location filename="../plugins/core/mail/popovers/MessageActionPopover.qml" line="44"/>
        <source>Reply all</source>
        <translation type="unfinished">Responder a todos</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/MessageActionPopover.qml" line="52"/>
        <source>Forward</source>
        <translation type="unfinished">Reenviar</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/MessageActionPopover.qml" line="65"/>
        <source>Move</source>
        <translation type="unfinished">Mover</translation>
    </message>
</context>
<context>
    <name>MessageHeader</name>
    <message>
        <location filename="../plugins/core/mail/messageview/MessageHeader.qml" line="47"/>
        <source>Hide details</source>
        <translation type="unfinished">Ocultar detalles</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/messageview/MessageHeader.qml" line="47"/>
        <source>View details</source>
        <translation type="unfinished">Ver detalles</translation>
    </message>
</context>
<context>
    <name>MessageListActionPopover</name>
    <message>
        <location filename="../plugins/core/mail/popovers/MessageListActionPopover.qml" line="44"/>
        <source>Mark as unread</source>
        <translation type="unfinished">Marcar como non lido</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/MessageListActionPopover.qml" line="44"/>
        <source>Mark as read</source>
        <translation type="unfinished">Marcar como lido</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/MessageListActionPopover.qml" line="52"/>
        <source>Mark as not important</source>
        <translation type="unfinished">Marcar non importante</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/MessageListActionPopover.qml" line="52"/>
        <source>Mark as important</source>
        <translation type="unfinished">Marcar importante</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/MessageListActionPopover.qml" line="60"/>
        <source>Mark as spam</source>
        <translation type="unfinished">Marcar como lixo</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/MessageListActionPopover.qml" line="73"/>
        <source>To-do</source>
        <translation type="unfinished">Tarefas pendentes</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/MessageListActionPopover.qml" line="89"/>
        <source>Done</source>
        <translation type="unfinished">Feito</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/MessageListActionPopover.qml" line="103"/>
        <source>Reply</source>
        <translation type="unfinished">Responder</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/MessageListActionPopover.qml" line="111"/>
        <source>Reply all</source>
        <translation type="unfinished">Responder a todos</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/MessageListActionPopover.qml" line="119"/>
        <source>Forward</source>
        <translation type="unfinished">Reenviar</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/MessageListActionPopover.qml" line="133"/>
        <source>Move</source>
        <translation type="unfinished">Mover</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/MessageListActionPopover.qml" line="142"/>
        <source>Restore to %1</source>
        <translation type="unfinished">Restabelecer a %1</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/MessageListActionPopover.qml" line="151"/>
        <source>Delete</source>
        <translation type="unfinished">Eliminar</translation>
    </message>
</context>
<context>
    <name>MessageListDelegate</name>
    <message>
        <location filename="../plugins/core/mail/delegates/MessageListDelegate.qml" line="50"/>
        <source>Un-mark flagged</source>
        <translation type="unfinished">Desmarcar etiquetado</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/delegates/MessageListDelegate.qml" line="50"/>
        <source>Mark flagged</source>
        <translation type="unfinished">Marcar etiquetado</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/delegates/MessageListDelegate.qml" line="59"/>
        <source>Mark as un-read</source>
        <translation type="unfinished">Marcar como non lido</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/delegates/MessageListDelegate.qml" line="59"/>
        <source>Mark as read</source>
        <translation type="unfinished">Marcar como lido</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/delegates/MessageListDelegate.qml" line="66"/>
        <source>Move message</source>
        <translation type="unfinished">Mover mensaxe</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/delegates/MessageListDelegate.qml" line="71"/>
        <source>Context menu</source>
        <translation type="unfinished">Menú contextual</translation>
    </message>
</context>
<context>
    <name>MessageListView</name>
    <message>
        <location filename="../plugins/core/mail/views/MessageListView.qml" line="114"/>
        <source>Unselect all</source>
        <translation type="unfinished">Deseleccionar todas</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/views/MessageListView.qml" line="114"/>
        <source>Select all</source>
        <translation type="unfinished">Seleccionar todas</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/views/MessageListView.qml" line="126"/>
        <source>Star</source>
        <translation type="unfinished">Estrela</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/views/MessageListView.qml" line="126"/>
        <source>Remove star</source>
        <translation type="unfinished">Eliminar estrela</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/views/MessageListView.qml" line="130"/>
        <source>Mark as un-read</source>
        <translation type="unfinished">Marcar como non lido</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/views/MessageListView.qml" line="130"/>
        <source>Mark as read</source>
        <translation type="unfinished">Marcar como lido</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/views/MessageListView.qml" line="137"/>
        <source>Delete</source>
        <translation type="unfinished">Eliminar</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/views/MessageListView.qml" line="332"/>
        <source>Load more messages ...</source>
        <translation type="unfinished">Cargar máis mensaxes...</translation>
    </message>
</context>
<context>
    <name>MessageViewContextMenu</name>
    <message>
        <location filename="../plugins/core/mail/popovers/MessageViewContextMenu.qml" line="46"/>
        <source>Open in browser</source>
        <translation type="unfinished">Abrir no navegador</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/MessageViewContextMenu.qml" line="54"/>
        <source>Copy link</source>
        <translation type="unfinished">Copiar ligazón</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/MessageViewContextMenu.qml" line="63"/>
        <source>Share link</source>
        <translation type="unfinished">Compartir ligazón</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/MessageViewContextMenu.qml" line="77"/>
        <source>Reply</source>
        <translation type="unfinished">Responder</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/MessageViewContextMenu.qml" line="86"/>
        <source>Reply all</source>
        <translation type="unfinished">Responder a todos</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/MessageViewContextMenu.qml" line="94"/>
        <source>Forward</source>
        <translation type="unfinished">Reenviar</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/MessageViewContextMenu.qml" line="106"/>
        <source>View source</source>
        <translation type="unfinished">Ver a fonte</translation>
    </message>
</context>
<context>
    <name>NavMenuAccountSettingsModel</name>
    <message>
        <location filename="../plugins/core/mail/models/NavMenuAccountSettingsModel.qml" line="29"/>
        <source>Manage accounts</source>
        <translation type="unfinished">Xestionar contas</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/models/NavMenuAccountSettingsModel.qml" line="45"/>
        <source>Display settings</source>
        <translation type="unfinished">Axustes da visualización</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/models/NavMenuAccountSettingsModel.qml" line="67"/>
        <source>Privacy settings</source>
        <translation type="unfinished">Axustes de privacidade</translation>
    </message>
</context>
<context>
    <name>NavMenuContactsModel</name>
    <message>
        <location filename="../plugins/core/mail/models/NavMenuContactsModel.qml" line="27"/>
        <source>Addressbook</source>
        <translation type="unfinished">Axenda</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/models/NavMenuContactsModel.qml" line="43"/>
        <source>Recent contacts</source>
        <translation type="unfinished">Contactos recentes</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/models/NavMenuContactsModel.qml" line="58"/>
        <source>Import contacts</source>
        <translation type="unfinished">Importar contactos</translation>
    </message>
</context>
<context>
    <name>NavMenuDekkoVisualModel</name>
    <message>
        <location filename="../plugins/core/mail/models/NavMenuDekkoVisualModel.qml" line="27"/>
        <source>Version</source>
        <translation type="unfinished">Versión</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/models/NavMenuDekkoVisualModel.qml" line="45"/>
        <source>Licenses</source>
        <translation type="unfinished">Licenzas</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/models/NavMenuDekkoVisualModel.qml" line="63"/>
        <source>Contributors</source>
        <translation type="unfinished">Contribuidores</translation>
    </message>
</context>
<context>
    <name>NavMenuModel</name>
    <message>
        <location filename="../plugins/core/mail/models/NavMenuModel.qml" line="98"/>
        <source>Smart folders</source>
        <translation type="unfinished">Cartafoles intelixentes</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/models/NavMenuModel.qml" line="120"/>
        <source>Folders</source>
        <translation type="unfinished">Cartafoles</translation>
    </message>
</context>
<context>
    <name>NavMenuPage</name>
    <message>
        <location filename="../plugins/core/mail/views/NavMenuPage.qml" line="30"/>
        <location filename="../plugins/core/mail/views/NavMenuPage.qml" line="64"/>
        <source>Mail</source>
        <translation type="unfinished">Correo</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/views/NavMenuPage.qml" line="67"/>
        <source>Contacts</source>
        <translation type="unfinished">Contactos</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/views/NavMenuPage.qml" line="70"/>
        <source>Settings</source>
        <translation type="unfinished">Axustes</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/views/NavMenuPage.qml" line="73"/>
        <source>About</source>
        <translation type="unfinished">Sobre</translation>
    </message>
</context>
<context>
    <name>NavMenuStandardFolderDelegate</name>
    <message>
        <location filename="../plugins/core/mail/delegates/NavMenuStandardFolderDelegate.qml" line="192"/>
        <source>Inbox (%1)</source>
        <translation type="unfinished">Caixa de entrada (%1)</translation>
    </message>
</context>
<context>
    <name>NavSideBar</name>
    <message>
        <location filename="../imports/components/private/NavSideBar.qml" line="155"/>
        <location filename="../plugins/core/mail/views/NavSideBar.qml" line="155"/>
        <source>Smart folders</source>
        <translation type="unfinished">Cartafoles intelixentes</translation>
    </message>
    <message>
        <location filename="../imports/components/private/NavSideBar.qml" line="169"/>
        <location filename="../plugins/core/mail/views/NavSideBar.qml" line="169"/>
        <source>Folders</source>
        <translation type="unfinished">Cartafoles</translation>
    </message>
</context>
<context>
    <name>NavViewContextMenu</name>
    <message>
        <location filename="../plugins/core/mail/popovers/NavViewContextMenu.qml" line="44"/>
        <source>Sync folder</source>
        <translation type="unfinished">Sincronizar cartafol</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/NavViewContextMenu.qml" line="53"/>
        <source>Send pending</source>
        <translation type="unfinished">Enviar pendentes</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/NavViewContextMenu.qml" line="70"/>
        <source>Mark folder read</source>
        <translation type="unfinished">Marcar cartafol como lido</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/NavViewContextMenu.qml" line="79"/>
        <source>Mark all done</source>
        <translation type="unfinished">Marcar todo como feito</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/NavViewContextMenu.qml" line="101"/>
        <source>Empty trash</source>
        <translation type="unfinished">Baleirar o lixo</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/NavViewContextMenu.qml" line="110"/>
        <source>Folder properties</source>
        <translation type="unfinished">Propiedades do cartafol</translation>
    </message>
</context>
<context>
    <name>NewAccountsUI</name>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/NewAccountsUI.qml" line="32"/>
        <source>New account</source>
        <translation type="unfinished">Nova conta</translation>
    </message>
</context>
<context>
    <name>NoAccountsUI</name>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/NoAccountsUI.qml" line="30"/>
        <source>Accounts</source>
        <translation type="unfinished">Contas</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/NoAccountsUI.qml" line="92"/>
        <source>No email account is setup.</source>
        <translation type="unfinished">Non hai ningunha conta de correo configurada.</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/NoAccountsUI.qml" line="101"/>
        <source>Add now</source>
        <translation type="unfinished">Engadir agora</translation>
    </message>
</context>
<context>
    <name>NotesStage</name>
    <message>
        <location filename="../plugins/core/notes/NotesStage.qml" line="34"/>
        <source>Coming soon</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NothingSelectedPage</name>
    <message>
        <location filename="../plugins/core/mail/views/NothingSelectedPage.qml" line="55"/>
        <source>No message selected</source>
        <translation type="unfinished">Non seleccionou ningunha mensaxe</translation>
    </message>
</context>
<context>
    <name>OutgoingServerGroup</name>
    <message>
        <location filename="../plugins/core/mail/settings/OutgoingServerGroup.qml" line="28"/>
        <source>Outgoing Server</source>
        <translation type="unfinished">Servidor saínte</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/OutgoingServerGroup.qml" line="75"/>
        <source>Hostname</source>
        <translation type="unfinished">Nome do equipo</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/OutgoingServerGroup.qml" line="83"/>
        <source>Port</source>
        <translation type="unfinished">Porto</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/OutgoingServerGroup.qml" line="92"/>
        <source>Username</source>
        <translation type="unfinished">Nome do usuario</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/OutgoingServerGroup.qml" line="100"/>
        <location filename="../plugins/core/mail/settings/OutgoingServerGroup.qml" line="103"/>
        <source>Password</source>
        <translation type="unfinished">Contrasinal</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/OutgoingServerGroup.qml" line="110"/>
        <source>Show password</source>
        <translation type="unfinished">Mostrar o contrasinal</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/OutgoingServerGroup.qml" line="116"/>
        <source>Security settings</source>
        <translation type="unfinished">Axustes de seguranza</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/OutgoingServerGroup.qml" line="149"/>
        <source>Authenticate from server capabilities</source>
        <translation type="unfinished">Autenticar desde as capacidades do servidor</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/OutgoingServerGroup.qml" line="155"/>
        <source>Allow untrusted certificates</source>
        <translation type="unfinished">Permitir certificados sen confianza</translation>
    </message>
</context>
<context>
    <name>PrivacySettings</name>
    <message>
        <location filename="../plugins/core/mail/settings/PrivacySettings.qml" line="28"/>
        <source>Message content</source>
        <translation type="unfinished">Contido da mensaxe</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/PrivacySettings.qml" line="32"/>
        <source>Allow remote content</source>
        <translation type="unfinished">Permitir contido remoto</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/settings/PrivacySettings.qml" line="42"/>
        <source>Auto load images</source>
        <translation type="unfinished">Cargar imaxes automaticamente</translation>
    </message>
</context>
<context>
    <name>PrivacySettingsPage</name>
    <message>
        <location filename="../plugins/core/mail/settings/PrivacySettingsPage.qml" line="21"/>
        <source>Privacy Settings</source>
        <translation type="unfinished">Axustes de privacidade</translation>
    </message>
</context>
<context>
    <name>PrivacySettingsPopup</name>
    <message>
        <location filename="../plugins/core/mail/settings/PrivacySettingsPopup.qml" line="21"/>
        <source>Privacy Settings</source>
        <translation type="unfinished">Axustes de privacidade</translation>
    </message>
</context>
<context>
    <name>RecipientField</name>
    <message>
        <location filename="../plugins/core/mail/composer/RecipientField.qml" line="97"/>
        <source>Enter an address</source>
        <translation type="unfinished">Escribir o enderezo</translation>
    </message>
</context>
<context>
    <name>RecipientInfo</name>
    <message>
        <location filename="../plugins/core/mail/messageview/RecipientInfo.qml" line="41"/>
        <source>Back</source>
        <translation type="unfinished">Volver</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/messageview/RecipientInfo.qml" line="93"/>
        <source>Copy to clipboard</source>
        <translation type="unfinished">Copiar ao portapapeis</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/messageview/RecipientInfo.qml" line="107"/>
        <source>Add to addressbook</source>
        <translation type="unfinished">Engadir á axenda</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/messageview/RecipientInfo.qml" line="122"/>
        <source>Send message</source>
        <translation type="unfinished">Enviar mensaxe</translation>
    </message>
</context>
<context>
    <name>RecipientInputContextMenu</name>
    <message>
        <location filename="../plugins/core/mail/composer/RecipientInputContextMenu.qml" line="55"/>
        <source>Add CC</source>
        <translation type="unfinished">Engadir Cc</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/composer/RecipientInputContextMenu.qml" line="68"/>
        <source>Add BCC</source>
        <translation type="unfinished">Engadir Cco</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/composer/RecipientInputContextMenu.qml" line="80"/>
        <source>Add contact</source>
        <translation type="unfinished">Engadir contacto</translation>
    </message>
</context>
<context>
    <name>RecipientPopover</name>
    <message>
        <location filename="../plugins/core/mail/popovers/RecipientPopover.qml" line="80"/>
        <source>Copy to clipboard</source>
        <translation type="unfinished">Copiar ao portapapeis</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/RecipientPopover.qml" line="89"/>
        <source>Add to addressbook</source>
        <translation type="unfinished">Engadir á axenda</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/RecipientPopover.qml" line="98"/>
        <source>Send message</source>
        <translation type="unfinished">Enviar mensaxe</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/popovers/RecipientPopover.qml" line="108"/>
        <source>Remove</source>
        <translation type="unfinished">Eliminar</translation>
    </message>
</context>
<context>
    <name>SenderIdentityField</name>
    <message>
        <location filename="../plugins/core/mail/composer/SenderIdentityField.qml" line="53"/>
        <source>From:</source>
        <translation type="unfinished">De:</translation>
    </message>
</context>
<context>
    <name>ServerDetails</name>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/ServerDetails.qml" line="69"/>
        <source>Hostname</source>
        <translation type="unfinished">Nome do equipo</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/ServerDetails.qml" line="78"/>
        <source>Port</source>
        <translation type="unfinished">Porto</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/ServerDetails.qml" line="100"/>
        <source>Username</source>
        <translation type="unfinished">Nome do usuario</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/ServerDetails.qml" line="109"/>
        <location filename="../plugins/core/mail/setupwizard/components/ServerDetails.qml" line="112"/>
        <source>Password</source>
        <translation type="unfinished">Contrasinal</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/ServerDetails.qml" line="119"/>
        <source>Show password</source>
        <translation type="unfinished">Mostrar o contrasinal</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/ServerDetails.qml" line="140"/>
        <source>Allow untrusted certificates</source>
        <translation type="unfinished">Permitir certificados sen confianza</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../plugins/core/settings/Settings.qml" line="11"/>
        <source>Settings</source>
        <translation type="unfinished">Axustes</translation>
    </message>
</context>
<context>
    <name>SettingsWindow</name>
    <message>
        <location filename="../plugins/core/settings/SettingsWindow.qml" line="26"/>
        <source>Dekko Settings</source>
        <translation type="unfinished">Axustes de Dekko</translation>
    </message>
</context>
<context>
    <name>SetupWizardWindow</name>
    <message>
        <location filename="../plugins/core/mail/setupwizard/SetupWizardWindow.qml" line="20"/>
        <source>Mail Setup Wizard</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SmartFolderDelegate</name>
    <message>
        <location filename="../plugins/core/mail/delegates/SmartFolderDelegate.qml" line="155"/>
        <source>Inbox (%1)</source>
        <translation type="unfinished">Caixa de entrada (%1)</translation>
    </message>
</context>
<context>
    <name>SubjectField</name>
    <message>
        <location filename="../plugins/core/mail/composer/SubjectField.qml" line="56"/>
        <source>Subject:</source>
        <translation type="unfinished">Asunto:</translation>
    </message>
</context>
<context>
    <name>SyncState</name>
    <message>
        <location filename="../plugins/core/mail/setupwizard/states/SyncState.qml" line="35"/>
        <source>Synchronizing account.</source>
        <translation type="unfinished">Sincronizando conta.</translation>
    </message>
</context>
<context>
    <name>TitledTextField</name>
    <message>
        <location filename="../imports/components/TitledTextField.qml" line="61"/>
        <source> (Required)</source>
        <translation type="unfinished"> (Requirido)</translation>
    </message>
</context>
<context>
    <name>UserInputUI</name>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/UserInputUI.qml" line="35"/>
        <source>Name</source>
        <translation type="unfinished">Nome</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/UserInputUI.qml" line="36"/>
        <source>Full name</source>
        <translation type="unfinished">Nome completo</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/UserInputUI.qml" line="43"/>
        <source>Description</source>
        <translation type="unfinished">Descrición</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/UserInputUI.qml" line="44"/>
        <source>E.g Home, Work...</source>
        <translation type="unfinished">P.e. Casa, Traballo...</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/UserInputUI.qml" line="51"/>
        <source>Email address</source>
        <translation type="unfinished">Enderezo de correo-e</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/UserInputUI.qml" line="53"/>
        <source>email@example.org</source>
        <translation type="unfinished">correo@exemplo.org</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/UserInputUI.qml" line="59"/>
        <location filename="../plugins/core/mail/setupwizard/components/UserInputUI.qml" line="62"/>
        <source>Password</source>
        <translation type="unfinished">Contrasinal</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/UserInputUI.qml" line="68"/>
        <source>Show password</source>
        <translation type="unfinished">Mostrar o contrasinal</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/UserInputUI.qml" line="78"/>
        <source>Cancel</source>
        <translation type="unfinished">Cancelar</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/UserInputUI.qml" line="82"/>
        <source>Next</source>
        <translation type="unfinished">Seguinte</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/UserInputUI.qml" line="124"/>
        <source>Password empty</source>
        <translation type="unfinished">Contrasinal en branco</translation>
    </message>
    <message>
        <location filename="../plugins/core/mail/setupwizard/components/UserInputUI.qml" line="124"/>
        <source>Would you like to continue?</source>
        <translation type="unfinished">Desexa continuar?</translation>
    </message>
</context>
<context>
    <name>ValidationState</name>
    <message>
        <location filename="../plugins/core/mail/setupwizard/states/ValidationState.qml" line="36"/>
        <source>Validating credentials.</source>
        <translation type="unfinished">Validando credenciais.</translation>
    </message>
</context>
<context>
    <name>VersionDialog</name>
    <message>
        <location filename="../imports/dialogs/VersionDialog.qml" line="25"/>
        <source>Version</source>
        <translation type="unfinished">Versión</translation>
    </message>
    <message>
        <location filename="../imports/dialogs/VersionDialog.qml" line="29"/>
        <source>Close</source>
        <translation type="unfinished">Pechar</translation>
    </message>
</context>
</TS>
